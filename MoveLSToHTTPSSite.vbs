Const OverwriteExisting = TRUE

set objWShell=wScript.createObject("WScript.Shell")
Set objFSO = CreateObject("Scripting.FileSystemObject")

strFolder = objWShell.expandEnvironmentStrings("%USERPROFILE%") & "\AppData\Roaming\Opera Software\Opera Stable\Local Storage\"

CopyAndReplaceFiles objFSO, strFolder

strFolder = objWShell.expandEnvironmentStrings("%USERPROFILE%") & "\AppData\Local\Google\Chrome\User Data\Default\Local Storage\"

CopyAndReplaceFiles objFSO, strFolder

Sub CopyAndReplaceFiles(FSO, Folder)

  If (FSO.FolderExists(Folder)) Then
    For Each oFile In FSO.GetFolder(Folder).Files
      If InStr(1, oFile.Name, "http_worldoftanks") = 1 Then
        FSO.CopyFile oFile, Folder & "https" & Mid(oFile.Name, 5, len(oFile.Name)), OverwriteExisting
      End If
    Next
  End If
    
End Sub
