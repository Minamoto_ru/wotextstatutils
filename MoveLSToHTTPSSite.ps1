Get-ChildItem "$env:USERPROFILE\AppData\Roaming\Opera Software\Opera Stable\Local Storage\http_worldoftanks*" |ForEach-Object {
    $NewName = $_.Name -replace "http",'https'
    $Destination = Join-Path -Path $_.Directory.FullName -ChildPath $NewName
    Copy-Item -Path $_.FullName -Destination $Destination -Force
}